const path = require('path');
const {randomNumber} = require('../helpers/libs');
const fs = require('fs-extra');
const md5 = require('md5');

const {image} = require('../models');
const {comment} = require('../models');

const sidebar = require('../helpers/sidebar');
const ctrl = {};

ctrl.index = async(req, res) => {
    let viewModel = {Image: {}, Comments: {}};

    const Image = await image.findOne({filename: {$regex: req.params.image_id}});
    if(Image){
        Image.views = Image.views + 1;
        viewModel.Image = Image;
        await Image.save();
        const Comments = await comment.find({image_id: Image._id});
        viewModel.Comments = Comments;
        viewModel = await sidebar(viewModel);
        res.render('Image', viewModel);
    }else{
        res.redirect('/');
    }
};

ctrl.create = (req, res) => {

    const saveImage = async () => {

        const imgUrl = randomNumber();
        const images = await image.find({filename: imgUrl});
        if(images.length > 0){;
            saveImage();
        }else{

            console.log(imgUrl);
            const imgtempath = req.file.path;
            const ext = path.extname(req.file.originalname).toLowerCase();
            const targetPath = path.resolve(`src/public/upload/${imgUrl}${ext}`)

            if(ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif'){
                await fs.rename(imgtempath, targetPath);
                const newImg = new image({
                    title: req.body.title,
                    filename: imgUrl + ext,
                    description: req.body.description
                });
                const imgSaved = await newImg.save();
                res.redirect('/images/' + imgUrl);
                //res.send('Subido');
                
            }else{
                await fs.unlink(imgtempath);
                res.status(500).json({error: 'Solo las imagenes estan permitidas'});
            }
        }
    };

    saveImage();
    
};


ctrl.like = async(req, res) => {
    const Image = await image.findOne({filename: {$regex: req.params.image_id}})
    if(Image){
        Image.likes = Image.likes + 1;
        await Image.save();
        res.json({likes: Image.likes});
    }else{
        res.status(500).json({error: 'error interno'});
    }
};


ctrl.comment = async(req, res) => {
    const Image = await image.findOne({filename: {$regex: req.params.image_id}});
    if(Image){
        const newComment = new comment(req.body);
        newComment.gravatar = md5(newComment.email);
        newComment.image_id = Image._id;
        await newComment.save();
        res.redirect('/images/' + Image.uniqueId);
    }else{
        res.redirect('/')
    }
    
};


ctrl.remove = async(req, res) => {  
    const Image = await image.findOne({filename: {$regex: req.params.image_id}});
    if(Image){
        await fs.unlink(path.resolve('./src/public/upload/' + Image.filename));
        await comment.deleteOne({image_id: Image._id});
        await Image.remove();
        res.json(true);
    }
};
    
    


module.exports = ctrl;