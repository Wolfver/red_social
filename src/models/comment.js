const {Schema, model} = require('mongoose');
const { comment } = require('../controllers/image');
const ObjectId = Schema.ObjectId;

const CommentSchema = new Schema({
    image_id: {type: ObjectId},
    email: {type: String},
    name: {type: String},
    gravatar: {type: String},
    comment: {type: String},
    timestamp: {type: Date, default: Date.now}
});

CommentSchema.virtual('Image')
    .set(function(Image){
        this._Image = Image;
    })
    .get(function(){
        return this._Image;
    })


module.exports = model('Comment', CommentSchema);