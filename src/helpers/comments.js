const {comment, image} = require('../models');
module.exports = {
    async newest(){
        const comments = await comment.find()
            .limit(5)
            .sort({timestamp: -1})
        
        for(const Comment of comments){
            const Image = await image.findOne({_id: Comment.image_id});
            Comment.Image = Image;
        }
        return comments;
    }
}